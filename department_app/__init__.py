import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from flask_restful import Api


logging.basicConfig(filename="demo.log", level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler())
logging.getLogger('waitress')
db = SQLAlchemy()
migrate = Migrate()


def create_app(config_class=Config):
    """
    Function for creating an flask app
    :param config_class: config class with properties of app
    :return: created app
    """

    app = Flask(__name__)
    app.config.from_object(config_class)
    api = Api(app)

    # Check if database already exist if no - create a new one
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    if not database_exists(engine.url):
        create_database(engine.url)

    db.init_app(app)
    migrate.init_app(app, db, directory="department_app/migrations")

    from department_app.rest.departments import DepartmentsResource
    from department_app.rest.department import DepartmentResource
    from department_app.rest.employees import EmployeesResource
    from department_app.rest.employee import EmployeeResource

    api.add_resource(DepartmentsResource, "/api/1.0/departments")
    api.add_resource(DepartmentResource, "/api/1.0/department")
    api.add_resource(EmployeesResource, "/api/1.0/employees")
    api.add_resource(EmployeeResource, "/api/1.0/employee")

    from department_app.views import departments
    from department_app.views import employees
    from department_app.views import department
    from department_app.views import employee

    app.register_blueprint(departments.bp, url_prefix="/")
    app.register_blueprint(department.bp, url_prefix="/")
    app.register_blueprint(employees.bp, url_prefix="/")
    app.register_blueprint(employee.bp, url_prefix="/")

    return app
