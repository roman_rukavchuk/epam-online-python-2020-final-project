import unittest
from department_app import create_app, db
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"


class TestRest(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        db.session.remove()
        db.drop_all()

    def test_empty_departments(self):
        expected_response = []
        response = self.client.get("/api/1.0/departments")

        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_employees_empty(self):
        expected_response = []
        response = self.client.get("/api/1.0/employees")

        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    # Test employee
    def test_get_employee_without_id(self):
        response = self.client.get("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_get_employee_empty(self):
        response = self.client.get("/api/1.0/employee", json={'id': 1})
        self.assertEqual(404, response.status_code, )

    def test_put_employee_without_id(self):
        response = self.client.put("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_post_employee_without_id(self):
        response = self.client.post("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_delete_employee_without_id(self):
        response = self.client.delete("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_get_department_without_name(self):
        response = self.client.get("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_put_department_without_name(self):
        response = self.client.put("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_post_department_without_name(self):
        response = self.client.post("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_delete_department_without_name(self):
        response = self.client.delete("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    # Test department
    def test_get_department_without_id(self):
        response = self.client.get("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_put_department_without_id(self):
        response = self.client.put("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_post_department_without_id(self):
        response = self.client.post("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_delete_department_without_id(self):
        response = self.client.delete("/api/1.0/department")
        self.assertEqual(400, response.status_code)
