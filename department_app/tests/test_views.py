import unittest
from department_app import create_app, db
from config import Config
from department_app.example_data_db import db_load_example_data
from werkzeug.exceptions import NotFound, Conflict, BadRequest


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"


class TestRest(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        # Populate database with data
        db_load_example_data(self.app, db)
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        db.session.remove()
        db.drop_all()

    # Departments
    def test_departments_get(self):
        response = self.client.get("/")
        self.assertEqual(200, response.status_code)

    def test_departments_get_2(self):
        response = self.client.get("/departments")
        self.assertEqual(200, response.status_code)

    def test_departments_search(self):
        response = self.client.post("/departments?name=Zoo")
        self.assertEqual(200, response.status_code)

    def test_departments_delete_bad(self):
        with self.assertRaises(BadRequest):
            self.client.post("/departments/delete")

    def test_departments_delete_bad_parameters(self):
        expected = (
            b'<!DOCTYPE html>\n<html lang="en">\n<head>\n    '
            b'<meta charset="UTF-8">\n    <'
            b'title>ERROR 409!</title>\n</head>\n<body>\n    <h1>ERROR 409</h1>'
            b'\n    <h2>T'
            b'here are employees in such department. You cannot delete it!</h2>'
            b'\n</body'
            b'>\n</html>')
        response = self.client.post("/departments/delete",
                                    content_type="multipart/form-data",
                                    data={"name": "Zoology"},
                                    follow_redirects=True)
        self.assertEqual(expected, response.get_data())

    def test_departments_delete_correct(self):
        response = self.client.post("/departments/delete",
                                    content_type="multipart/form-data",
                                    data={"name": "Geography"},
                                    follow_redirects=True)
        self.assertEqual(200, response.status_code)

    # Test Employees
    def test_employees_get(self):
        response = self.client.get("/employees")
        self.assertEqual(200, response.status_code)

    def test_employees_search(self):
        response = self.client.post("/employees?begin_date=2000-12-12")
        self.assertEqual(200, response.status_code)

    def test_employees_search2(self):
        response = self.client.post("/employees?end_date=2000-12-12")
        self.assertEqual(200, response.status_code)

    def test_employees_delete_bad(self):
        with self.assertRaises(BadRequest):
            self.client.post("/employees/delete")

    def test_employees_delete_bad2(self):
        expected = (
            b'<!DOCTYPE html>\n<html lang="en">\n<head>\n    '
            b'<meta charset="UTF-8">\n    <'
            b'title>ERROR 404!</title>\n</head>\n<body>\n    '
            b'<h1>ERROR 404</h1>\n    <h2>E'
            b'rror! Cannot delete employee!</h2>\n</body>\n</html>')
        response = self.client.post("/employees/delete",
                                    content_type="multipart/form-data",
                                    data={"id": "abs"},
                                    follow_redirects=True
                                    )
        self.assertEqual(expected, response.get_data())

    def test_employees_delete_bad3(self):
        response = self.client.post("/employees/delete",
                                    content_type="multipart/form-data",
                                    data={"id": 10},
                                    follow_redirects=True
                                    )
        self.assertEqual(404, response.status_code)

    def test_employees_delete_correct(self):
        response = self.client.post("/employees/delete",
                                    content_type="multipart/form-data",
                                    data={"id": 4},
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)

    # Test employee
    def test_employee_get(self):
        with self.assertRaises(TypeError):
            self.client.get("/edit_employee")

    def test_employee_get2(self):
        expected = (
            b'<!DOCTYPE html>\n<html lang="en">\n<head>\n    '
            b'<meta charset="UTF-8">\n    <'
            b'title>ERROR 400!</title>\n</head>\n<body>\n    '
            b'<h1>ERROR 400</h1>\n    <h2>Y'
            b'ou should give correct id of employee!</h2>\n</body>\n</html>')
        response = self.client.get("/edit_employee?id=10")
        self.assertEqual(expected, response.get_data())

    def test_employee_get_correct(self):
        response = self.client.get("/edit_employee?id=1")
        self.assertEqual(200, response.status_code)

    def test_employee_edit(self):
        response = self.client.post("/edit_employee",
                                    content_type="multipart/form-data",
                                    data={
                                        "id": 4,
                                        "first_name": "test",
                                        "last_name": "test",
                                        "department_name": "Zoology",
                                        "date_of_birth": "2000-1-1",
                                        "salary": 1
                                    },
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)

    def test_employee_edit_bad(self):
        response = self.client.post("/edit_employee",
                                    content_type="multipart/form-data",
                                    data={
                                        "id": 4,
                                        "first_name": "test",
                                        "last_name": "test",
                                        "department_name": "Zoology",
                                        "date_of_birth": "2000-1-1",
                                        "salary": -100
                                    },
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)

    def test_employee_create(self):
        response = self.client.get("/create_employee")
        self.assertEqual(200, response.status_code)

    def test_employee_create_bad(self):
        with self.assertRaises(BadRequest):
            self.client.post("/create_employee",
                             content_type="multipart/form-data",
                             data={
                                 "last_name": "test",
                                 "department_name": "Zoology",
                                 "date_of_birth": "2000-1-1",
                                 "salary": 100
                             },
                             follow_redirects=True
                             )

    def test_employee_create_correct(self):
        response = self.client.post("/create_employee",
                                    content_type="multipart/form-data",
                                    data={
                                        "first_name": "test",
                                        "last_name": "test",
                                        "department_name": "Zoology",
                                        "date_of_birth": "2000-1-1",
                                        "salary": 100
                                    },
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)

    # Test department
    def test_department_get(self):
        response = self.client.get("/edit_department")
        self.assertEqual(200, response.status_code)

    def test_department_get_correct(self):
        response = self.client.get("/edit_department?name=Zoology")
        self.assertEqual(200, response.status_code)

    def test_department_bad_edit(self):
        with self.assertRaises(BadRequest):
            response = self.client.post("/edit_department",
                                        content_type="multipart/form-data",
                                        data={
                                            "name": "Zoology"
                                        },
                                        follow_redirects=True
                                        )

    def test_department_correct_edit(self):
        response = self.client.post("/edit_department",
                                    content_type="multipart/form-data",
                                    data={
                                        "name": "Zoology",
                                        "address": "Test"
                                    },
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)

    def test_department_get_create(self):
        response = self.client.get("/create_department")
        self.assertEqual(200, response.status_code)

    def test_department_create_incorrect(self):
        response = self.client.post("/create_department",
                                    content_type="multipart/form-data",
                                    data={
                                        "name": "Zoology",
                                        "address": "Test"
                                    },
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)

    def test_department_create_correct(self):
        response = self.client.post("/create_department",
                                    content_type="multipart/form-data",
                                    data={
                                        "name": "Test",
                                        "address": "Test"
                                    },
                                    follow_redirects=True
                                    )
        self.assertEqual(200, response.status_code)
