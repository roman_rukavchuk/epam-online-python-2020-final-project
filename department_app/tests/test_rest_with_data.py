import unittest
import department_app.rest.employee as employee
import department_app.rest.departments as departments
from department_app import create_app, db
from datetime import date
from department_app.models.models import Employee, Department
from config import Config
from department_app.example_data_db import db_load_example_data
from werkzeug.exceptions import NotFound


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"


class TestRest(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        # Populate database with data
        db_load_example_data(self.app, db)
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        db.session.remove()
        db.drop_all()

    # Test department
    def test_departments_get(self):
        expected_response = [{'address': 'Narodna sqr. 2, 223 room',
                              'avg_salary': 900.0,
                              'count_employees': 1,
                              'name': 'Zoology'},
                             {'address': 'Narodna sqr. 2, 103 room',
                              'avg_salary': 1000.0,
                              'count_employees': 2,
                              'name': 'Microbiology'},
                             {'address': 'Voloshyna str., 23',
                              'avg_salary': 1500.0,
                              'count_employees': 1,
                              'name': 'Botanic'},
                             {'address': 'Voloshyna str., 23',
                              'avg_salary': 0.0,
                              'count_employees': 0,
                              'name': 'Geography'}]
        response = self.client.get("/api/1.0/departments")

        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_department_get_search(self):
        expected_response = [{
            'address': 'Narodna sqr. 2, 223 room',
            'avg_salary': 900.0,
            'count_employees': 1,
            'name': 'Zoology'}]
        response = departments.DepartmentsResource(search_name="Zoology").get()
        self.assertEqual(expected_response, response.get_json())

    # Test employees
    def test_employees(self):
        expected_response = [{'date_of_birth': '13.03.1976',
                              'department_name': 'Zoology',
                              'first_name': 'Ivan',
                              'id': 1,
                              'last_name': 'Ivanov',
                              'salary': 900.0},
                             {'date_of_birth': '12.12.1960',
                              'department_name': 'Botanic',
                              'first_name': 'Roman',
                              'id': 2,
                              'last_name': 'Shah',
                              'salary': 1500.0},
                             {'date_of_birth': '01.06.1990',
                              'department_name': 'Microbiology',
                              'first_name': 'Dmytro',
                              'id': 3,
                              'last_name': 'Bado',
                              'salary': 1500.0},
                             {'date_of_birth': '01.11.1950',
                              'department_name': 'Microbiology',
                              'first_name': 'Oleh',
                              'id': 4,
                              'last_name': 'Morkryi',
                              'salary': 500.0}]

        response = self.client.get("/api/1.0/employees")
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_employees_begin_date(self):
        expected_response = [{'date_of_birth': '01.06.1990',
                              'department_name': 'Microbiology',
                              'first_name': 'Dmytro',
                              'id': 3,
                              'last_name': 'Bado',
                              'salary': 1500.0}]

        response = self.client.get("/api/1.0/employees?begin_date=1989-12-12")
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_employees_end_date(self):
        expected_response = [{'date_of_birth': '01.11.1950',
                              'department_name': 'Microbiology',
                              'first_name': 'Oleh',
                              'id': 4,
                              'last_name': 'Morkryi',
                              'salary': 500.0}]

        response = self.client.get("/api/1.0/employees?end_date=1951-01-01")
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_employees_incorrect_date(self):
        response = self.client.get("/api/1.0/employees?end_date=1051-01-01")
        self.assertEqual([], response.get_json())

    # Test employee
    def test_get_employee_without_id(self):
        response = self.client.get("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_get_incorrect_employee_id(self):
        response = self.client.get("/api/1.0/employee", json={"id": 0})
        self.assertEqual(404, response.status_code)

    def test_get_correct_employee_id(self):
        expected_response = {'date_of_birth': '1976-03-13',
                             'department_name': 'Zoology',
                             'first_name': 'Ivan',
                             'id': 1,
                             'last_name': 'Ivanov',
                             'salary': 900.0}
        response = self.client.get("/api/1.0/employee", json={"id": 1})
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_get_correct_employee_name(self):
        expected_response = {'date_of_birth': '1976-03-13',
                             'department_name': 'Zoology',
                             'first_name': 'Ivan',
                             'id': 1,
                             'last_name': 'Ivanov',
                             'salary': 900.0}
        response = self.client.get("/api/1.0/employee",
                                   json={"first_name": "Ivan", "last_name":
                                       "Ivanov"})
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_response, response.get_json())

    def test_put_employee_without_id(self):
        response = self.client.put("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_put_edit_employee(self):
        new_employee = {
            "first_name": "First",
            "last_name": "Last",
            "department_name": "Microbiology",
            "date_of_birth": "2000-12-12",
            "salary": 100.0
        }
        response = self.client.put("/api/1.0/employee", json=new_employee)

        self.assertEqual(201, response.status_code)

    def test_put_edit_first_name(self):
        employee_json = {
            "id": 1,
            "first_name": "Test"
        }
        response = self.client.put("/api/1.0/employee", json=employee_json)

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            employee_json["first_name"],
            Employee.query.filter(Employee.id ==
                                  employee_json["id"]).first().first_name
        )

    def test_put_edit_last_name(self):
        employee_json = {
            "id": 1,
            "last_name": "Test"
        }
        response = self.client.put("/api/1.0/employee", json=employee_json)

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            employee_json["last_name"],
            Employee.query.filter(Employee.id ==
                                  employee_json["id"]).first().last_name
        )

    def test_put_edit_department(self):
        department_json = {
            "id": 1,
            "department_name": "Botanic"
        }
        response = self.client.put("/api/1.0/employee", json=department_json)

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            department_json["department_name"],
            Employee.query.filter(Employee.id ==
                                  department_json["id"]).first().department_name
        )

    def test_put_incorrect_department(self):
        response = self.client.put("/api/1.0/department", json={"name": "XYZ"})
        self.assertEqual(400, response.status_code)

    def test_put_incorrect_date(self):
        response = self.client.put("/api/1.0/employee",
                                   json={"id": 1, "date_of_birth": "1000-1-1"})
        self.assertEqual(404, response.status_code)

    def test_put_incorrect_date2(self):
        response = self.client.put("/api/1.0/employee",
                                   json={"id": 1, "date_of_birth": "2030-1-2"})
        self.assertEqual(404, response.status_code)

    def test_put_incorrect_date3(self):
        response = self.client.put("/api/1.0/employee",
                                   json={"id": 1, "date_of_birth": "100-1-33"})
        self.assertEqual(404, response.status_code)

    def test_put_incorrect_date4(self):
        response = self.client.put("/api/1.0/employee",
                                   json={"id": 1, "date_of_birth": "1000-1/1"})
        self.assertEqual(404, response.status_code)

    def test_put_edit_date(self):
        employee_json = {
            "id": 1,
            "date_of_birth": "2019-12-12"
        }
        response = self.client.put("/api/1.0/employee", json=employee_json)

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            date(2019, 12, 12),
            Employee.query.filter(Employee.id ==
                                  employee_json["id"]).first().date_of_birth
        )

    def test_put_edit_date_2(self):
        employee_json = {
            "id": 1,
            "date_of_birth": "12.12.2019"
        }
        response = self.client.put("/api/1.0/employee", json=employee_json)

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            date(2019, 12, 12),
            Employee.query.filter(Employee.id ==
                                  employee_json["id"]).first().date_of_birth
        )

    def test_put_edit_salary(self):
        employee_json = {
            "id": 1,
            "salary": 10000.0
        }
        response = self.client.put("/api/1.0/employee", json=employee_json)

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            float(employee_json["salary"]),
            float(Employee.query.filter(Employee.id ==
                                        employee_json["id"]).first().salary
                  )
        )

    def test_put_incorrect_salary(self):
        response = self.client.put("/api/1.0/employee",
                                   json={"id": 1, "salary": -100})
        self.assertEqual(404, response.status_code)

    def test_post_employee_without_id(self):
        response = self.client.post("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_post_employee(self):
        new_employee = {
            "first_name": "First",
            "last_name": "Last",
            "department_name": "Microbiology",
            "date_of_birth": "2000-12-12",
            "salary": 100.0
        }
        response = self.client.post("/api/1.0/employee", json=new_employee)

        self.assertEqual(201, response.status_code)

    def test_delete_employee_without_id(self):
        response = self.client.delete("/api/1.0/employee")
        self.assertEqual(404, response.status_code)

    def test_delete_employee(self):
        response = self.client.delete("/api/1.0/employee", json={"id": 2})
        self.assertEqual(200, response.status_code)

    # Test department
    def test_get_department_without_id(self):
        response = self.client.get("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_get_department_incorrect_name(self):
        response = self.client.get("/api/1.0/department", json={"name": "Ast"})
        self.assertEqual(404, response.status_code)

    def test_get_department_(self):
        expected = {
            'address': 'Narodna sqr. 2, 103 room',
            'name': 'Microbiology'
        }
        response = self.client.get("/api/1.0/department",
                                   json={"name": "Microbiology"})
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected, response.get_json())

    def test_put_department_without_name(self):
        response = self.client.put("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_put_department_incorrect_name(self):
        response = self.client.put("/api/1.0/department", json={"name": "XYZ"})
        self.assertEqual(400, response.status_code)

    def test_put_department_name_less_than_3(self):
        expected = {
            'address': 'Narodna sqr. 2, 103 room',
            'name': 'Te'
        }
        response = self.client.put("/api/1.0/department", json=expected)
        self.assertEqual(400, response.status_code)

    def test_put_department_without_address(self):
        expected = {
            'name': 'Tell'
        }
        response = self.client.put("/api/1.0/department", json=expected)
        self.assertEqual(400, response.status_code)

    def test_put_deparmtment_correct_all(self):
        expected = {
            'address': 'Narodna sqr. 2, 103 room',
            'name': 'Test'
        }
        response = self.client.put("/api/1.0/department", json=expected)
        self.assertEqual(201, response.status_code)

    def test_put_department_correct(self):
        expected = {
            'address': 'Narodna sqr. 2, 103 room',
            'name': 'Test'
        }
        response = self.client.put("/api/1.0/department", json=expected)
        self.assertEqual(201, response.status_code)
        self.assertEqual(
            expected["name"],
            Department.query.filter(Department.name ==
                                    expected["name"]).first().name
        )

    def test_post_department_without_address(self):
        expected = {
            'name': 'Test'
        }
        response = self.client.post("/api/1.0/department", json=expected)
        self.assertEqual(400, response.status_code)

    def test_post_department_name_less_than_3(self):
        expected = {
            'address': 'Narodna sqr. 2, 103 room',
            'name': 'Te'
        }
        response = self.client.post("/api/1.0/department", json=expected)
        self.assertEqual(400, response.status_code)

    def test_post_department_without_id(self):
        response = self.client.post("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_delete_department_without_id(self):
        response = self.client.delete("/api/1.0/department")
        self.assertEqual(400, response.status_code)

    def test_delete_department(self):
        self.client.put("api/1.0/department",
                        json={"name": "Tst", "address": "Y"})
        response = self.client.delete("/api/1.0/department",
                                      json={"name": "Tst"}
                                      )
        self.assertEqual(200, response.status_code)

    def test_delete_department_wit_employees(self):
        response = self.client.delete("/api/1.0/department",
                                      json={"name": "Zoology"})
        self.assertEqual(409, response.status_code)

    # Test other functions
    def test_check_correct_employee_id(self):
        expected = Employee.query.filter(Employee.id == 1).first()
        actual = employee.check_correct_employee_id(1)
        self.assertEqual(expected, actual)

    def test_check_correct_employee_id_incorrect_id1(self):
        with self.assertRaises(NotFound):
            employee.check_correct_employee_id("")

    def test_check_correct_employee_id_incorrect_id2(self):
        with self.assertRaises(NotFound):
            employee.check_correct_employee_id(10)

    def test_search_by_name(self):
        self.assertEqual(employee.search_by_name("Ivan", "Ivanov"),
                         Employee.query.filter(Employee.id == 1).first())

    def test_search_by_name_put(self):
        self.assertIsNone(employee.search_by_name("Test", "Test", method="PUT"))

    def test_search_by_name_incorrect_name(self):
        with self.assertRaises(NotFound):
            employee.search_by_name("", "")

    def test_search_by_name_incorrect_name2(self):
        with self.assertRaises(NotFound):
            employee.search_by_name("test", "test")

    def test_to_date_incorrect(self):
        with self.assertRaises(NotFound):
            employee.to_date("12/12/2000")

    def test_to_date_incorrect2(self):
        with self.assertRaises(NotFound):
            employee.to_date("2000.2.2")

    def test_to_date_incorrect3(self):
        with self.assertRaises(NotFound):
            employee.to_date("12/12/2000")

    def test_to_date_incorrect4(self):
        with self.assertRaises(NotFound):
            employee.to_date("2000-2.2")

    def test_to_date_correct(self):
        self.assertEqual(date(1903, 12, 12), employee.to_date("1903-12-12"))
        self.assertEqual(date(1903, 12, 12), employee.to_date("12.12.1903"))

    def test_count_employees(self):
        self.assertEqual(departments.count_of_employees("Botanic"), 1)

    def test_count_employees2(self):
        self.assertEqual(departments.count_of_employees("Microbiology"), 2)

    def test_avg_salary(self):
        self.assertEqual(departments.avg_salary("Botanic"), 1500.0)

    def test_avg_salary2(self):
        self.assertEqual(departments.avg_salary("Microbiology"), 1000.0)
