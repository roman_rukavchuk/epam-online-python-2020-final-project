from department_app import db


class Department(db.Model):
    name = db.Column(db.String(90), primary_key=True)
    address = db.Column(db.String(255), nullable=False)
    employee = db.relationship("Employee", backref="department")


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(90), nullable=False, index=True)
    last_name = db.Column(db.String(90), nullable=False, index=True)
    department_name = db.Column(db.String(90), db.ForeignKey(Department.name),
                                nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    salary = db.Column(db.Numeric(10, 2), nullable=False)
