from flask import jsonify
from flask_restful import Resource, reqparse, abort
from department_app.models.models import Employee, Department
from department_app import db
from datetime import date, datetime
from decimal import Decimal

parser = reqparse.RequestParser()
parser.add_argument("id", type=int)
parser.add_argument("first_name", type=str)
parser.add_argument("last_name", type=str)
parser.add_argument("department_name", type=str)
parser.add_argument("date_of_birth", type=str)
parser.add_argument("salary", type=Decimal)


class EmployeeResource(Resource):
    """
    Class for employee and rest
    """

    def __init__(self, id_=None, first_name=None, last_name=None,
                 department_name=None, date_of_birth=None, salary=None):
        args = parser.parse_args()
        self.id = id_ or parser.parse_args().get("id")
        self.first_name = first_name or args.get("first_name")
        self.last_name = last_name or args.get("last_name")
        self.department_name = department_name or args.get("department_name")
        self.date_of_birth = date_of_birth or args.get("date_of_birth")
        self.salary = salary or args.get("salary")

    def get(self):
        """
        Find and return employee in rest
        :return:
        """
        if self.id:
            employee = check_correct_employee_id(self.id)
        elif self.first_name and self.last_name:
            employee = search_by_name(self.first_name, self.last_name)
        else:
            abort(404, message="Please enter correct data!")

        return jsonify({
            "id": employee.id,
            "first_name": employee.first_name,
            "last_name": employee.last_name,
            "department_name": employee.department_name,
            "date_of_birth": employee.date_of_birth.strftime("%Y-%m-%d"),
            "salary": float(employee.salary)
        })

    def put(self):
        """
        Modify and create employee in rest
        :return: status code
        """

        if self.id:
            employee = check_correct_employee_id(self.id)
        elif self.first_name and self.last_name:
            employee = search_by_name(self.first_name, self.last_name,
                                      method="PUT")
        else:
            abort(404, message="Please enter correct data!")

        if self.date_of_birth:
            self.date_of_birth = to_date(self.date_of_birth)

        self._check_correct_fields_employee()
        if not employee:
            if self._check_all_fields():
                employee = Employee(
                    first_name=self.first_name,
                    last_name=self.last_name,
                    department_name=self.department_name,
                    date_of_birth=self.date_of_birth,
                    salary=self.salary
                )
                db.session.add(employee)
                db.session.commit()
                return "", 201
            else:
                abort(404, message="To create new employee you should give "
                                   "all fields")

        if self.first_name:
            employee.first_name = self.first_name

        if self.last_name:
            employee.last_name = self.last_name

        if self.department_name:
            employee.department_name = self.department_name

        if self.date_of_birth:
            employee.date_of_birth = self.date_of_birth

        if self.salary:
            employee.salary = self.salary

        db.session.commit()
        return "", 200

    def post(self):
        """
        Method for post rest
        :return: status code
        """

        if self.date_of_birth:
            self.date_of_birth = to_date(self.date_of_birth)

        self._check_correct_fields_employee()

        if self._check_all_fields():
            employee = Employee(
                first_name=self.first_name,
                last_name=self.last_name,
                department_name=self.department_name,
                date_of_birth=self.date_of_birth,
                salary=self.salary
            )
            db.session.add(employee)
            db.session.commit()
            return "", 201
        else:
            abort(404, message="To create new employee you should give "
                               "all fields")

    def delete(self):
        """
        Delete employee
        :return: status code
        """

        employee = check_correct_employee_id(self.id)
        db.session.delete(employee)
        db.session.commit()
        return "", 200

    def _check_all_fields(self):
        """
        Checks if given args are enough to create new employee
        :return: True if correct False if not
        """

        args = [self.first_name, self.last_name,
                self.department_name, self.date_of_birth,
                self.salary]

        return all(args)

    def _check_correct_fields_employee(self):
        """
        Check if given arguments are correct to add into employee
        :return: None
        """

        if self.date_of_birth:
            if date(1903, 1, 2) > self.date_of_birth or \
                    self.date_of_birth > datetime.today().date():
                abort(404, message="Please enter correct date!")

        if self.salary:
            if Decimal(self.salary) < 0:
                abort(404, message="Please enter correct salary!")

        departments = [department.name for department in Department.query.all()]
        if self.department_name:
            if self.department_name not in departments:
                abort(404, message="Please enter correct department name!")


def check_correct_employee_id(employee_id):
    """
    Check if employee with given id exist
    :param employee_id: id of employee
    :return: data employee of with id
    """

    if not employee_id or employee_id <= 0:
        abort(404, message="Enter valid id")

    employee = Employee.query.filter(Employee.id == employee_id).first()

    if not employee:
        abort(404, message="Enter id of existing employee!")

    return employee


def search_by_name(fist_name, last_name, method=None):
    """
    Search employee by given first and last name
    :param method: for PUT method, if no id given we could change or create
    employee
    :param fist_name: first name of employee
    :param last_name: last name of employee
    :return: employee of given fist name and last name
    """

    if not fist_name or not last_name:
        abort(404, message="Enter correct name!")

    employee = Employee.query.filter(Employee.first_name == fist_name).filter(
        Employee.last_name == last_name).all()

    if not employee and method == "PUT":
        return None
    elif not employee:
        abort(404, message="There is no employee with such name!")
    elif len(employee) > 1:
        abort(404, message="There is more than one employee with such name!")

    return employee[0]


def to_date(date_string):
    """
    Convert string to date
    :param date_string: string wit date
    :return: date
    """
    if "-" in date_string:
        try:
            return datetime.strptime(date_string, "%Y-%m-%d").date()
        except ValueError as err:
            abort(404, message="Please enter correct date in format Y-m-D!")
    elif "." in date_string:
        try:
            return datetime.strptime(date_string, "%d.%m.%Y").date()
        except ValueError:
            abort(404, message="Please enter correct date in format d.m.Y!")
    else:
        abort(404, message="You should use Y-M-D or D.M.Y format for "
                           "date")
