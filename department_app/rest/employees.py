from flask import jsonify
from flask_restful import Resource, reqparse
from department_app.models.models import Employee
from datetime import datetime, date
from department_app.rest.employee import to_date

parser = reqparse.RequestParser()
parser.add_argument("department_name", type=str)
parser.add_argument("begin_date", type=str)
parser.add_argument("end_date", type=str)


class EmployeesResource(Resource):
    """
    Class employees in rest
    """

    def __init__(self, department_name=None, begin_date=None, end_date=None):
        args = parser.parse_args()
        self.department_name = department_name or args.get(
            "department_name")
        # For filter employees by date
        if begin_date:
            self.begin_date = to_date(begin_date)
        elif args.get("begin_date"):
            self.begin_date = to_date(args.get("begin_date"))
        else:
            self.begin_date = date(1903, 1, 2)
        if end_date:
            self.end_date = to_date(end_date)
        elif args.get("end_date"):
            self.end_date = to_date(args.get("end_date"))
        else:
            self.end_date = datetime.today().date()

    def get(self):
        """
        Get list and return it
        :return: json with employees
        """

        if self.department_name:
            employees = Employee.query.filter(Employee.department_name ==
                                              self.department_name).all()
        else:
            employees = Employee.query.all()
        employees_list = []

        for employee in employees:
            if employee.date_of_birth > self.begin_date and \
                    employee.date_of_birth < self.end_date:
                employees_list.append({
                    "id": employee.id,
                    "first_name": employee.first_name,
                    "last_name": employee.last_name,
                    "department_name": employee.department_name,
                    "date_of_birth": employee.date_of_birth.strftime(
                        "%d.%m.%Y"),
                    "salary": float(employee.salary)
                })

        return jsonify(employees_list)
