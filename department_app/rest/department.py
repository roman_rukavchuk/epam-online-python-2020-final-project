from flask import jsonify
from flask_restful import Resource, reqparse, abort
from department_app.models.models import Department, Employee
from department_app import db

parser = reqparse.RequestParser()
parser.add_argument("name", type=str)
parser.add_argument("address", type=str)


class DepartmentResource(Resource):
    """
    Class for manipulate department using rest
    """
    def __init__(self, name=None, address=None):
        args = parser.parse_args()
        self.name = name or args.get("name")
        self.address = address or args.get("address")

    def get(self):
        """
        Get department with given name and return data about it
        :return: error message or data about department
        """

        department = check_department_name(self.name)
        return jsonify({"name": department.name, "address": department.address})

    def put(self):
        """
        Update, create department for rest
        :return: status code
        """

        department = check_department_name(self.name, method="PUT")
        if not department and not self.address:
            abort(400, message="For create department name and address "
                               "should be given")
        elif not department:
            # Valid data for name > 3
            if len(self.name) < 3:
                abort(400, message="Length of name of department should be 3 "
                                   "or more")
            new_department = Department(name=self.name, address=self.address)
            db.session.add(new_department)
            db.session.commit()
            return "", 201
        elif not self.address:
            abort(400, message="For modify department address should be given")

        department.address = self.address
        db.session.commit()
        return "", 200

    def post(self):
        """
        Method for rest post method
        :return: status code
        """

        department = check_department_name(self.name, method="PUT")
        if department:
            abort(400, message="This department already exist")

        if not self.address:
            abort(400, message="For create department name and address "
                               "should be given")

        if len(self.name) < 3:
            abort(400, message="Length of name of department should be 3 "
                               "or more")
        new_department = Department(name=self.name, address=self.address)
        db.session.add(new_department)
        db.session.commit()
        return "", 201

    def delete(self):
        """
        Delete department for rest
        :return: 200 if correct
        """
        department = check_department_name(name=self.name)
        if Employee.query.filter(Employee.department_name ==
                                 department.name).count() != 0:
            abort(409, message="There are employees in this department")
        db.session.delete(department)
        db.session.commit()
        return "", 200


def check_department_name(name, method=None):
    """
    Check if name is given or department exits
    :param method: for checking if department name could be none
    :param name: name of department
    :return: department data
    """

    if not name:
        abort(400, message="Department name should be given!")

    department = Department.query.filter_by(name=name).first()

    if method == "PUT" and not department:
        return None
    elif not department:
        abort(404, message=f"Department {name} doesn't exist!")

    return department
