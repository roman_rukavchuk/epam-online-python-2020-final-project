from flask import jsonify
from flask_restful import Resource
from department_app.models.models import Department, Employee
from sqlalchemy import func


class DepartmentsResource(Resource):
    """
    Class for manage rest for all Departments
    """
    def __init__(self, search_name=None):
        self.search_name = search_name

    def get(self):
        """
        Reads all departments from database and return it to user
        :return: json list of all departments
        """
        if self.search_name:
            departments_list = Department.query.filter(Department.name.ilike(
                "%" + self.search_name + "%"))
        else:
            departments_list = Department.query.all()
        departments_list_json = []

        for department in departments_list:
            departments_list_json.append({"name": department.name,
                                          "address": department.address,
                                          "count_employees":
                                              count_of_employees(
                                                  department.name),
                                          "avg_salary": avg_salary(
                                              department.name)
                                          })

        return jsonify(departments_list_json)


def count_of_employees(department_name):
    """
    Finds count of employees in given department
    :param department_name: name of department to find
    :return: number of employees in department
    """
    return Employee.query.filter(Employee.department_name ==
                                 department_name).count()


def avg_salary(department_name):
    """
    Find average salary for department
    :param department_name: name of department
    :return: average salary
    """
    result = (Employee.query.with_entities(func.avg(Employee.salary)).filter(
        Employee.department_name == department_name).scalar())
    return round(float(result), 2) if result else 0.00
