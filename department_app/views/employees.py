from flask import Blueprint, render_template, request, redirect, url_for
from department_app.rest.employees import EmployeesResource
from department_app.rest.employee import EmployeeResource

bp = Blueprint("employees", __name__)


@bp.route("/employees", methods=["GET", "POST"])
def get_employees():
    """
    Function for handling get employees requests
    :return: render template
    """
    begin_date = request.form.get("begin_date")
    end_date = request.form.get("end_date")
    result_json = EmployeesResource(
        request.args.get("department_name"), begin_date=begin_date,
        end_date=end_date).get().get_json()
    return render_template("employees.html", result=result_json)


@bp.route("/employees/delete", methods=["POST"])
def delete_employee():
    """
    Delete employee from database
    :return: redirect to employees page
    """
    id_ = request.form["id"]
    if not id_:
        render_template(
            "errors.html",
            code=404,
            message="Id of the employee should be given"
        )
    try:
        id_ = int(id_)
        EmployeeResource(id_=id_).delete()
    except ValueError:
        return render_template("errors.html", code=404,
                               message="Error! Cannot delete employee!")
    return redirect(url_for("employees.get_employees"))
