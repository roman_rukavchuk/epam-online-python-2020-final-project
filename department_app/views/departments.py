from flask import Blueprint, redirect, render_template, url_for, request
from department_app.rest.departments import DepartmentsResource
from department_app.rest.department import DepartmentResource
from werkzeug.exceptions import Conflict


bp = Blueprint("departments", __name__)


@bp.route("/", methods=["GET"])
@bp.route("/departments", methods=["GET", "POST"])
def get_departments():
    """
    Function for handling get request to departments
    :return: render template
    """
    search = request.form.get("search_name")
    result_json = DepartmentsResource(search_name=search).get().get_json()
    return render_template("departments.html", result=result_json)


@bp.route("/departments/delete", methods=["POST"])
def delete_department():
    """
    Function for handling department delete
    :return: redirect to department page
    """
    name = request.form['name']
    if not name:
        return render_template("errors.html",
                               code=404,
                               message="ERROR Department name should be given!")
    try:
        DepartmentResource(name=name).delete()
    except Conflict:
        return render_template("errors.html", code=409,
                               message="There are employees in such department."
                                       " You cannot delete it!")
    return redirect(url_for("departments.get_departments"))

