from flask import Blueprint, redirect, render_template, request, url_for
from department_app.models.models import Department
from werkzeug.exceptions import BadRequest, Conflict, NotFound
from department_app.rest.employee import EmployeeResource

bp = Blueprint("employee", __name__)


@bp.route("/edit_employee", methods=["GET"])
def get_edit_employee():
    """
    Function to handle get request for edit employee
    :return: redirect to employees page
    """
    id_ = request.args.get("id")
    try:
        employee = EmployeeResource(id_=int(id_)).get().get_json()
        departments = Department.query.with_entities(Department.name)
    except (ValueError, BadRequest, NotFound):
        return render_template("errors.html", code=400, message="You should "
                                                                "give "
                                                                "correct id of "
                                                                "employee!")
    return render_template("edit_employee.html", employee=employee,
                           departments=departments)


@bp.route("/edit_employee", methods=["POST"])
def edit_employee():
    """
    Function for handling post request for edit employee
    :return: redirect to employees page
    """
    id_ = request.form["id"]
    first_name = request.form["first_name"]
    last_name = request.form["last_name"]
    department_name = request.form["department_name"]
    date_of_birth = request.form["date_of_birth"]
    salary = request.form["salary"]
    employee = {
        "id_": int(id_),
        "first_name": first_name,
        "last_name": last_name,
        "department_name": department_name,
        "date_of_birth": date_of_birth,
        "salary": salary
    }
    try:
        EmployeeResource(**employee).put()
    except (BadRequest, Conflict, NotFound) as error:
        # Always need departments for select option
        departments = Department.query.with_entities(Department.name)
        return render_template(
            "edit_employee.html",
            employee=employee,
            departments=departments,
            error=error.data.get("message") or "Please correct values!"
        )
    return redirect(url_for("employees.get_employees"))


@bp.route("/create_employee", methods=["GET"])
def get_create_employee():
    """
    Function for handling get request to form create employee
    :return:
    """
    departments = Department.query.with_entities(Department.name)
    return render_template("create_employee.html", departments=departments)


@bp.route("/create_employee", methods=["POST"])
def create_employee():
    """
    Function for handling post requests to create new emploee
    :return: redirect to employees page
    """
    first_name = request.form["first_name"]
    last_name = request.form["last_name"]
    department_name = request.form["department_name"]
    date_of_birth = request.form["date_of_birth"]
    salary = request.form["salary"]
    try:
        EmployeeResource(
            first_name=first_name,
            last_name=last_name,
            department_name=department_name,
            date_of_birth=date_of_birth,
            salary=salary).post()
    except (BadRequest, Conflict, NotFound) as error:
        # Always need departments for select option
        departments = Department.query.with_entities(Department.name)
        return render_template(
            "create_employee.html",
            departments=departments,
            error=error.data.get("message") or "Please correct values!"
        )
    return redirect(url_for("employees.get_employees"))
