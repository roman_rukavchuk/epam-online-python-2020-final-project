from flask import Blueprint, render_template, request, redirect
from department_app.rest.department import DepartmentResource
from werkzeug.exceptions import BadRequest, Conflict, NotFound

bp = Blueprint("department", __name__)


@bp.route("/edit_department", methods=["GET"])
def get_edit_department():
    """
    Function for handling get requests to change department
    :return: render template to edit department
    """
    department_name = request.args.get("name")
    try:
        department = DepartmentResource(name=department_name).get().get_json()
    except (BadRequest, NotFound):
        return render_template("errors.html", code=400,
                               message="Please chose department to edit!")
    return render_template("edit_department.html", department=department)


@bp.route("/create_department", methods=["GET"])
def get_create_department():
    """
    Function to handle get request for create departments
    :return: render template
    """
    return render_template("create_department.html")


@bp.route("/edit_department", methods=["POST"])
def edit_department():
    """
    Function to handle changes in departments
    :return: back to main page if all is correct
    """
    name = request.form["name"]
    address = request.form["address"]
    try:
        DepartmentResource(name=name, address=address).put()
    except (BadRequest, Conflict) as error:
        department = {"name": name, "address": address}
        return render_template("edit_department.html", department=department,
                               error=error.data.get(
                                   "message") or "Please correct values!")
    return redirect("/")


@bp.route("/create_department", methods=["POST"])
def create_department():
    """
    Function to handle creation department
    :return: back to main page if all is correct
    """
    name = request.form["name"]
    address = request.form["address"]
    try:
        DepartmentResource(name=name, address=address).post()
    except (BadRequest, Conflict) as error:
        return render_template("create_department.html",
                               error=error.data.get(
                                   "message") or "Please correct values!")
    return redirect("/")
