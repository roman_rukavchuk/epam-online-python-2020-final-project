from datetime import datetime
from department_app.models.models import Department, Employee


def db_load_example_data(app, db):
    """
    Populate db with test data
    :param app: flask app obj
    :param db: db flask obj
    :return: None
    """

    dep1 = Department(name="Zoology", address="Narodna sqr. 2, 223 room")
    dep2 = Department(name="Microbiology", address="Narodna sqr. 2, 103 room")
    dep3 = Department(name="Botanic", address="Voloshyna str., 23")
    dep4 = Department(name="Geography", address="Voloshyna str., 23")
    with app.app_context():
        db.session.add(dep1)
        db.session.add(dep2)
        db.session.add(dep3)
        db.session.add(dep4)
        db.session.commit()

    empl1 = Employee(
        first_name="Ivan",
        last_name="Ivanov",
        department_name="Zoology",
        date_of_birth=datetime(1976, 3, 13),
        salary=900
    )
    empl2 = Employee(
        first_name="Roman",
        last_name="Shah",
        department_name="Botanic",
        date_of_birth=datetime(1960, 12, 12),
        salary=1500
    )
    empl3 = Employee(
        first_name="Dmytro",
        last_name="Bado",
        department_name="Microbiology",
        date_of_birth=datetime(1990, 6, 1),
        salary=1500
    )

    empl4 = Employee(
        first_name="Oleh",
        last_name="Morkryi",
        department_name="Microbiology",
        date_of_birth=datetime(1950, 11, 1),
        salary=500
    )

    with app.app_context():
        db.session.add(empl1)
        db.session.add(empl2)
        db.session.add(empl3)
        db.session.add(empl4)
        db.session.commit()
