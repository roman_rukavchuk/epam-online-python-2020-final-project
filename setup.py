import sys
from setuptools import setup, find_packages

CURRENT_PYTHON = sys.version_info[:2]
REQUIRED_PYTHON = (3, 6)

if CURRENT_PYTHON < REQUIRED_PYTHON:
    sys.stderr.write("Unsupported Python version! You should use 3.8 and later")

setup(
    name='epam-online-python-2020-final-project',
    version='0.1',
    packages=find_packages(include=['department_app', 'department_app.*']),
    url='',
    license='',
    author='roman_rukavchuk',
    author_email='roman.rukavchuk@gmail.com',
    description='University Department Application',
    install_requires=[
        'alembic==1.4.3',
        'aniso8601==8.1.0',
        'astroid==2.4.2',
        'certifi==2020.12.5',
        'chardet==3.0.4',
        'click==7.1.2',
        'colorama==0.4.4',
        'coverage==4.5.4',
        'coveralls==2.2.0',
        'docopt==0.6.2',
        'flake8==3.8.4',
        'Flask==1.1.2',
        'Flask-Migrate==2.5.3',
        'Flask-RESTful==0.3.8',
        'Flask-SQLAlchemy==2.4.4',
        'idna==2.10',
        'isort==5.6.4',
        'itsdangerous==1.1.0',
        'Jinja2==2.11.2',
        'lazy-object-proxy==1.4.3',
        'Mako==1.1.3',
        'MarkupSafe==1.1.1',
        'mccabe==0.6.1',
        'mysql-connector-python==8.0.22',
        'mysqlclient==1.4.6',
        'protobuf==3.14.0',
        'pycodestyle==2.6.0',
        'pyflakes==2.2.0',
        'pylint==2.6.0',
        'python-dateutil==2.8.1',
        'python-dotenv==0.15.0',
        'python-editor==1.0.4',
        'pytz==2020.4',
        'PyYAML==5.3.1',
        'requests==2.25.0',
        'six==1.15.0',
        'SQLAlchemy==1.3.20',
        'SQLAlchemy-Utils==0.36.8',
        'toml==0.10.2',
        'urllib3==1.26.2',
        'Werkzeug==1.0.1',
        'wrapt==1.12.1']
)
