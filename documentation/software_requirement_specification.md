<h1 style="text-align: center;">University departments</h1>

"University departments" is a web-application which allows users to record and
 manipulate information about university departments and employees.
 
Application should provide:

* Storing departments, employees in a database;
* Display list of departments;
* Updating the list of departments (adding, editing, removing);
* Display list of employees;
* Updating the list of employees (adding, editing, removing);
* Search departments by its name;
* Search employees by date of hir/her birth.

## 1. Departments
### 1.1 Display list of departments

This mode is designed to view the list of departments and number of employees
 of each departments.
 
***Main scenario:***
* User select item *"Departments*;
* Application displays list of Departments;

![Departments list](./Departments_list.jpg)
*Fig 1. Departments list*
    

The list displays the following columns:
* Department name - unique name of the Department;
* Department address - place, where you can find the department;
* Number of employees - number of employees in this department;
* Average salary - average salary for this department

***Search by name:***
* User can search department by name, input the needed name and press enter or
 loop button.
* The application will display departments with such names or no departments
 if there is no department with such name.

### 1.2 Add department

***Main scenario:***
* User clicks *"Add department"* button;
* Application displays form to enter new department;
* User enters data and press *"Save"* button;
* If any data is entered incorrectly, message are displayed;
* If entered data is valid, then record is adding to database;
* If error occurs, then error message is displaying;
* If new department is successfully added, then list of department with added
 record is displaying.

***Cancel operation scenario:***
* User clicks *"Add department"* button;
* Application displays form to enter new department;
* User enters data and press *"Cancel"* button;
* Data don't save in database, list of departments is displaying to the user;
* If user selects the menu *Departments* or *"Employees"*, the data will not be
 saved to the database.

![Adding department](./Departments_add_new.jpg)
*Fig 1.2 Add new department form*

When adding a department, the following details are entered:
* Department name - full name of department;
* Department address - place, where you can find the department.

### 1.3 Edit Department

***Main scenario:***
* User clicks *"Edit"* button on the *Department list form* in the selected
 order line;
* Application displays form to edit department;
* User enters data and press *"Save"* button;
* If any data is entered incorrectly, message are displayed;
* If entered data is valid, then record changed in database;
* If error occurs, then error message is displaying;
* If edit department is successful, then list of department with added
 record is displaying.

***Cancel operation scenario:***
* User clicks *"Edit"* button on the *Department list form* in the selected
 order line;
* Application displays form to edit department;
* User enters data and press *"Cancel"* button;
* Data don't changed in database, list of departments is displaying to the user;
* If user selects the menu *Departments* or *"Employees"*, the data will not be
 saved to the database.

![Edit department](./Departments_edit.jpg)
*Fig 1.3 Edit department form*

When editing a department, the following details are entered:
* Department name - full name of department;
* Department address - place, where you can find the department.

Constraint for data validation:

* Department name - maximum length of 90 characters, minimum 3, unique;
* Department address - maximum length of 255 characters.

### 1.4 Delete Department

![Delete department](./Department_delete.jpg)
*Fig 1.4 Delete department*

***Main scenario:***
* User clicks *"Delete"* button on the *Department list form* in the selected
 order line;
* Confirmation dialog is displayed;
* The user confirms the removal;
* Record is deleter from database;
* If error occurs, then error message is displaying;
* If delete department is successful, then list of departments without deleted
 record is displaying.
 
 ***Cancel operation scenario:***
* User clicks *"Delete"* button on the *Department list form* in the selected
 order line;
* Confirmation dialog is displayed;
* The user rejects the removal;
* List of departments without changes is displaying.

## 2. Employees
### 2.1 Display list of employees

This mode is designed for viewing and editing the list of employees.

***Main scenario:***
* User select item *"Employees*;
* Application displays list of Employees;

![Departments list](./Employees_list.jpg)
*Fig 2.1. Employees list*

The list displays the following columns:
* First name - first name of employee;
* Last name - last name of employee;;
* Department name - name of his/her department;
* Date of birth - date of employee birth;
* Salary - employee's salary in US dollars; 

***Search by date:***
* User can search employee by date of birth, input start and or end date 
  and press search.
* The application will display employees with birth date in given range or 
  no empoyees if there is no empoyees with such birth dates.

### 2.2 Add employee

***Main scenario:***
* User clicks *"Add employee"* button;
* Application displays form to enter new employee;
* User enters data and press *"Save"* button;
* If any data is entered incorrectly, message are displayed;
* If entered data is valid, then record is adding to database;
* If error occurs, then error message is displaying;
* If new employee is successfully added, then list of employees with added
 record is displaying.

***Cancel operation scenario:***
* User clicks *"Add employee"* button;
* Application displays form to enter new employee;
* User enters data and press *"Cancel"* button;
* Data don't save in database, list of employees is displaying to the user;
* If user selects the menu *Departments* or *"Employees"*, the data will not be
 saved to the database.

![Adding employee](./Employee_add_new.jpg)
*Fig 2.2. Add new employee form*

When adding an employee, the following details are entered:
* First name - first name of employee;
* Last name - last name of employee;;
* Department name - name of his/her department, selected from given list;
* Date of birth - date of employee birth;
* Salary - employee's salary in US dollars.

Constrains:
* First name - not null, required, maximum length 90 characters;
* Last name - not null, required, maximum length 90 characters;
* Department name - selected from given list, required;
* Date of birth - date in format dd.mm.yyyy later then 02.01.1903 and earlier
 then current date, required;
* Salary - > 0, required.

### 2.3 Edit employee

***Main scenario:***
* User clicks *"Edit"* button on the *Employees list form* in the selected
 order line;
* Application displays form to edit employee;
* User enters data and press *"Save"* button;
* If any data is entered incorrectly, message are displayed;
* If entered data is valid, then record is changed in database;
* If error occurs, then error message is displaying;
* If edit employee is successfully, then list of employees with added
 record is displaying.

***Cancel operation scenario:***
* User clicks *"Edit"* button on the *Employees list form* in the selected
 order line;
* Application displays form to edit employee;
* User enters data and press *"Cancel"* button;
* Data don't save in database, list of employees is displaying to the user;
* If user selects the menu *Departments* or *"Employees"*, the data will not be
 saved to the database.

![Edit employee](./Employees_edit.jpg)
*Fig 2.3. Edit employee form*


### 2.4 Delete Employee

***Main scenario:***
* User clicks *"Delete"* button on the *Employees list form* in the selected
 order line;
* Confirmation dialog is displayed;
* The user confirms the removal;
* Record is deleted from database;
* If error occurs, then error message is displaying;
* If delete employee is successful, then list of employees without deleted
 record is displaying.
 
 ***Cancel operation scenario:***
* User clicks *"Delete"* button on the *Employee list form* in the selected
 order line;
* Confirmation dialog is displayed;
* The user rejects the removal;
* List of employees without changes is displaying.

![Delete employee](./Employee_delete.png)
*Fig 2.4. Delete employee*

## 3. Api
### 3.1 Departments
User can get all departments using /api/1.0/departments in json format, by 
default all, if parameter **search_name** is given only departments with 
corresponding names.

### 3.2 Employees
User can get all employees using /api/1.0/employees in json format, by 
default all, if parameter **begin_date** or **end_date** is given only 
employees with corresponding dates of birth. User can also specify 
**department_name** parameter -  returns only employees of such department.


### 3.3 Department
User can get department using /api/1.0/department specify **name** parameter,
if no - error is given.
User can change department address using *PUT* method, specify **name** and 
**address** parameters.
User can add new department using *POST* method, specify **name** and 
**address** parameters.
User can delete department using *DELETE* method, specify **name** parameter 
if no employees in this department, else - error is given.

### 3.4 Employee
User can get/create/edit employee using /api/1.0/employee.
For *GET* employee **id** or **first_name** and **second_name** should be 
given. If no - error.
For create new employee *POST* method should be used, specifying all parameters:
**first_name**, **last_name**, **department_name**, **date_of_birth**, 
**salary**. If all parameters are not specified - error is given.
For edit employee *PUT* method should be used, specifying **id** parameter 
or **first_name** and **last_name**, given parameters that should be changed.
For delete employee *DELETE* method should be used, specifiying **id** 
parameter.