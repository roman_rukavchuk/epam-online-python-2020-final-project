# EPAM Online Python 2020 Final Project

REST and WEB application for managing university departments. For 
installation **pip install .** should be 
used.
By default, app using standard server, you can use gunicorn or waitress.
For correct work you can create run.py and run it:
```python
from department_app import create_app

app = create_app()


if __name__ == "__main__":
    app.run()

```

At the top level of app, **config.py** file should be created, for example:
```python
import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

```
